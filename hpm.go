/*
Copyright 2014 Patrick Borgeest

This file is part of HPM.

HPM is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

HPM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with HPM.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"bitbucket.org/pborgeest/hpm/generator"
	"bitbucket.org/pborgeest/hpm/lexer"
	"bitbucket.org/pborgeest/hpm/parser"
)

func main() {
	bytes, _ := ioutil.ReadAll(os.Stdin)
	lexer := lexer.Lex(string(bytes))
	root := parser.Parse(lexer)
	fmt.Println(generator.Generate(root))
}
