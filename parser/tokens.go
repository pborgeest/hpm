/*
Copyright 2014 Patrick Borgeest

This file is part of HPM.

HPM is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

HPM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with HPM.  If not, see <http://www.gnu.org/licenses/>.
*/

package parser

type AST interface {
	GetNodeType() int
	AddChild(AST)
	IsNil() bool
	String() string
	Token() Token
	Children() []AST
	GetInternal() string
}

type Token interface {
	Typ() int
	Text() string
	String() string
}

const (
	EOF           = -1
	INVALID_TOKEN = iota
	PAGE
	LEFT_CURLY
	RIGHT_CURLY
	LEFT_SQUARE
	RIGHT_SQUARE
	STYLE
	TITLE
	EQUALS
	QUOTE
	PRINTABLES_MINUS_QUOTE
	LEFTBLOCK
	RIGHTBLOCK
	LINK
	LEFT_PAREN
	RIGHT_PAREN
	PRINTABLES_MINUS_PAREN
	PROTOCOL
	VALID_URL_CHARS
	CONTENT
	BLOCK
	SECTION
)

func TypToString(typ int) string {
	strings := map[int]string{
		-1: "EOF",
		1:  "INVALID_TOKEN",
		2:  "PAGE",
		3:  "LEFT_CURLY",
		4:  "RIGHT_CURLY",
		5:  "LEFT_SQUARE",
		6:  "RIGHT_SQUARE",
		7:  "STYLE",
		8:  "TITLE",
		9:  "EQUALS",
		10: "QUOTE",
		11: "PRINTABLES_MINUS_QUOTE",
		12: "LEFTBLOCK",
		13: "RIGHTBLOCK",
		14: "LINK",
		15: "LEFT_PAREN",
		16: "RIGHT_PAREN",
		17: "PRINTABLES_MINUS_PAREN",
		18: "PROTOCOL",
		19: "VALID_URL_CHARS",
		20: "CONTENT",
		21: "BLOCK",
		22: "SECTION",
	}
	return strings[typ]
}
