/*
Copyright 2014 Patrick Borgeest

This file is part of HPM.

HPM is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

HPM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with HPM.  If not, see <http://www.gnu.org/licenses/>.
*/

package parser

import "fmt"

func (p *parser) match(token int) {
	if p.lookahead.Typ() == token {
		//	log.Print("--", p.lookahead)
		p.consume()
	} else {
		fmt.Errorf("Expected %s but got %s ", token, p.lookahead)
	}
}

func (p *parser) consume() { p.lookahead = p.lexer.NextToken() }

func (p *parser) lookaheadIs(token int) bool {
	return p.lookahead.Typ() == token
}
