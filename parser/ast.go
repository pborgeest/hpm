/*
Copyright 2014 Patrick Borgeest

This file is part of HPM.

HPM is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

HPM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with HPM.  If not, see <http://www.gnu.org/licenses/>.
*/

package parser

import (
	"fmt"
)

type past struct {
	token    Token
	children []AST
	internal string
}

func NewInternalAST(name string) AST {
	children := make([]AST, 0)
	return &past{children: children, internal: name}
}

// these bare strings must match those in generator/html.go
func NewNilAST() AST {
	return NewInternalAST("NIL")
}

func NewLinksAST() AST {
	return NewInternalAST("links")
}

func NewHeadAST() AST {
	return NewInternalAST("head")
}

func NewBodyAST() AST {
	return NewInternalAST("body")
}

func NewAST(root Token) AST {
	children := make([]AST, 0)
	return &past{token: root, children: children}
}
func (a *past) GetNodeType() int {
	return a.token.Typ()
}
func (a *past) Children() []AST {
	return a.children
}
func (a *past) Token() Token {
	return a.token
}
func (a *past) AddChild(t AST) {
	a.children = append(a.children, t)
}
func (a *past) IsNil() bool {
	return a.token == nil
}
func (a *past) GetInternal() string {
	return a.internal
}
func (a *past) String() string {
	childrenstring := ""
	for _, ch := range a.children {
		childrenstring = childrenstring + " " + ch.String()
	}
	if a.IsNil() {
		return fmt.Sprintf("(%s %s)", a.GetInternal(), childrenstring)
	} else {
		return fmt.Sprintf("(%s%s)", a.token.String(), childrenstring)
	}
}
