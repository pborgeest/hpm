/*
Copyright 2014 Patrick Borgeest

This file is part of HPM.

HPM is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

HPM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with HPM.  If not, see <http://www.gnu.org/licenses/>.
*/

package parser

func (p *parser) page() AST {
	root := NewAST(p.lookahead)
	p.match(PAGE)
	root.AddChild(p.pageAtts())
	p.match(LEFT_CURLY)
	root.AddChild(p.pageBody())
	p.match(RIGHT_CURLY)
	return root
}

func (p *parser) pageAtts() AST {
	root := NewHeadAST()
	p.match(LEFT_SQUARE)
	for p.lookaheadIs(STYLE) || p.lookaheadIs(TITLE) {
		root.AddChild(p.pageAtt())
	}
	p.match(RIGHT_SQUARE)
	return root
}

func (p *parser) pageAtt() AST {
	root := p.attName()
	p.match(EQUALS)
	root.AddChild(p.quotedString())
	return root
}

func (p *parser) attName() AST {
	if p.lookaheadIs(STYLE) {
		root := NewAST(p.lookahead)
		p.match(STYLE)
		return root
	}
	root := NewAST(p.lookahead)
	p.match(TITLE)
	return root
}

func (p *parser) quotedString() AST {
	p.match(QUOTE)
	root := NewAST(p.lookahead)
	p.match(PRINTABLES_MINUS_QUOTE)
	p.match(QUOTE)
	return root
}

func (p *parser) pageBody() AST {
	root := NewBodyAST()
	for p.lookaheadIs(LEFTBLOCK) || p.lookaheadIs(RIGHTBLOCK) {
		root.AddChild(p.sideblock())
	}
	root.AddChild(p.content())
	return root
}

func (p *parser) content() AST {
	root := NewAST(p.lookahead)
	p.match(CONTENT)
	p.match(LEFT_CURLY)
	for p.lookaheadIs(BLOCK) {
		root.AddChild(p.block())
	}
	p.match(RIGHT_CURLY)
	return root
}

func (p *parser) block() AST {
	root := NewAST(p.lookahead)
	p.match(BLOCK)
	root.AddChild(p.title())
	p.match(LEFT_CURLY)
	root.AddChild(p.matter())
	p.match(RIGHT_CURLY)
	return root
}

func (p *parser) matter() AST {
	root := NewNilAST()
	if p.lookaheadIs(SECTION) {
		root.AddChild(p.section())
		for p.lookaheadIs(SECTION) {
			root.AddChild(p.section())
		}
		if p.lookaheadIs(LINK) {
			root.AddChild(p.links())
		}
	} else {
		root.AddChild(p.links())
	}
	return root
}

func (p *parser) section() AST {
	root := NewAST(p.lookahead)
	p.match(SECTION)
	root.AddChild(p.title())
	p.match(LEFT_CURLY)
	root.AddChild(p.matter())
	p.match(RIGHT_CURLY)
	return root
}

func (p *parser) sideblock() AST {
	root := p.sideblockName()
	p.match(LEFT_CURLY)
	for p.lookaheadIs(LINK) {
		root.AddChild(p.links())
	}
	p.match(RIGHT_CURLY)
	return root
}

func (p *parser) sideblockName() AST {
	if p.lookaheadIs(LEFTBLOCK) {
		root := NewAST(p.lookahead)
		p.match(LEFTBLOCK)
		return root
	}
	root := NewAST(p.lookahead)
	p.match(RIGHTBLOCK)
	return root
}

func (p *parser) links() AST {
	root := NewLinksAST()
	root.AddChild(p.link())
	for p.lookaheadIs(LINK) {
		root.AddChild(p.link())
	}
	return root
}

func (p *parser) link() AST {
	root := NewAST(p.lookahead)
	p.match(LINK)
	root.AddChild(p.title())
	root.AddChild(p.url())
	return root
}

func (p *parser) title() AST {
	p.match(LEFT_PAREN)
	root := NewAST(p.lookahead)
	p.match(PRINTABLES_MINUS_PAREN)
	p.match(RIGHT_PAREN)
	return root
}

func (p *parser) url() AST {
	if p.lookaheadIs(QUOTE) {
		return p.quotedString()
	} else {
		return p.uri()
	}
}

func (p *parser) uri() AST {
	root := NewNilAST()
	root.AddChild(NewAST(p.lookahead))
	p.match(PROTOCOL)
	root.AddChild(p.validUrlChars())
	return root
}

func (p *parser) validUrlChars() AST {
	root := NewNilAST()
	for p.lookaheadIs(VALID_URL_CHARS) {
		root.AddChild(NewAST(p.lookahead))
		p.match(VALID_URL_CHARS)
	}
	return root
}
