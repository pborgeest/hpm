/*
Copyright 2014 Patrick Borgeest

This file is part of HPM.

HPM is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

HPM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with HPM.  If not, see <http://www.gnu.org/licenses/>.
*/

package parser

type Lexer interface {
	NextToken() Token
}

type parser struct {
	lexer     Lexer
	lookahead Token
}

func Parse(lexer Lexer) AST {
	parser := &parser{lexer: lexer, lookahead: lexer.NextToken()}
	return parser.page()
}
