/*
Copyright 2014 Patrick Borgeest

This file is part of HPM.

HPM is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

HPM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with HPM.  If not, see <http://www.gnu.org/licenses/>.
*/

package lexer

import (
	"fmt"
	"strings"

	"bitbucket.org/pborgeest/hpm/parser"
)

const (
	lexing = iota
	in_quote
	in_paren
	in_protocol
	in_slashslash_comment
	in_slashstar_comment
)

type lexer struct {
	input string // the string being scanned.
	start int    // current position in the input.
	pos   int    // current position in the input.
	state int
}

func Lex(input string) *lexer {
	return &lexer{input: input, state: lexing}
}

func (l *lexer) NextToken() parser.Token {
	for l.pos < len(l.input) {
		if l.state == in_quote && l.input[l.pos] != '"' {
			for l.input[l.pos] != '"' {
				l.pos = l.pos + 1
			}
			return l.foundUpToToken(parser.PRINTABLES_MINUS_QUOTE)
		}
		if l.state == in_paren && l.input[l.pos] != ')' {
			for l.input[l.pos] != ')' {
				l.pos = l.pos + 1
			}
			return l.foundUpToToken(parser.PRINTABLES_MINUS_PAREN)
		}
		if l.state == in_protocol {
			for valid_url_character(l.input[l.pos]) {
				l.pos = l.pos + 1
			}
			l.state = lexing
			return l.foundUpToToken(parser.VALID_URL_CHARS)
		}
		if found, token := l.findKeyword(); found {
			return token
		}
		switch l.input[l.pos] {
		case '/':
			l.COMMENT()
			return l.NextToken()
		case ' ', '\n', '\t', '\r':
			l.WS()
			return l.NextToken()
		case '{':
			return l.foundToken(parser.LEFT_CURLY)
		case '}':
			return l.foundToken(parser.RIGHT_CURLY)
		case '[':
			return l.foundToken(parser.LEFT_SQUARE)
		case ']':
			return l.foundToken(parser.RIGHT_SQUARE)
		case '(':
			l.state = in_paren
			return l.foundToken(parser.LEFT_PAREN)
		case ')':
			l.state = lexing
			return l.foundToken(parser.RIGHT_PAREN)
		case '=':
			return l.foundToken(parser.EQUALS)
		case '"':
			if l.state == in_quote {
				l.state = lexing
			} else {
				l.state = in_quote
			}
			return l.foundToken(parser.QUOTE)
		}

		fmt.Errorf("Bad token at %.20q...", l.input[l.pos-5:])
	}
	return NewToken(parser.EOF, "EOF")
}

func (l *lexer) foundToken(typ int) parser.Token {
	token := NewToken(typ, l.input[l.start:l.pos+1])
	l.pos = l.pos + 1
	l.start = l.pos
	return token
}

func (l *lexer) foundUpToToken(typ int) parser.Token {
	token := NewToken(typ, l.input[l.start:l.pos])
	l.pos = l.pos
	l.start = l.pos
	return token
}

func (l *lexer) WS() {
	c := l.input[l.pos]
	for c == ' ' || c == '\n' || c == '\t' || c == '\n' {
		l.pos = l.pos + 1
		if l.pos == len(l.input) {
			return
		}
		l.start = l.pos
		c = l.input[l.pos]
	}
}

func (l *lexer) COMMENT() {
	if strings.HasPrefix(l.input[l.pos:], "//") {
		l.consumeUntil("\n")
	}
	if strings.HasPrefix(l.input[l.pos:], "/*") {
		l.consumeUntil("*/")
	}
}

func (l *lexer) consumeUntil(until string) {
	l.pos = l.pos + len(until)
	for !strings.HasPrefix(l.input[l.pos:], until) {
		l.pos = l.pos + 1
	}
	l.pos = l.pos + len(until)
	l.start = l.pos
}

func (l *lexer) findKeyword() (bool, parser.Token) {
	table := []struct {
		typ  int
		text string
	}{
		{parser.PAGE, "page"},
		{parser.STYLE, "style"},
		{parser.TITLE, "title"},
		{parser.LEFTBLOCK, "leftblock"},
		{parser.RIGHTBLOCK, "rightblock"},
		{parser.LINK, "link"},
		{parser.CONTENT, "content"},
		{parser.BLOCK, "block"},
		{parser.SECTION, "section"},
	}
	for _, entry := range table {
		if strings.HasPrefix(l.input[l.pos:], entry.text) {
			token := NewToken(entry.typ, l.input[l.start:l.pos+len(entry.text)])
			l.pos = l.pos + len(entry.text)
			l.start = l.pos
			return true, token
		}
	}
	if strings.HasPrefix(l.input[l.pos:], "http") {
		protocollength := len("http")
		l.state = in_protocol
		if l.input[l.pos+protocollength] == 's' {
			protocollength = protocollength + 1
		}
		token := NewToken(parser.PROTOCOL, l.input[l.start:l.pos+protocollength])
		l.pos = l.pos + protocollength
		l.start = l.pos
		return true, token
	}
	return false, token{}

}

func valid_url_character(ch uint8) bool {
	switch ch {
	case ';', '/', '?', ':', '@', '+', '$', ',', '-', '_', '.', '!', '~', '*', '\'', '(', ')', '=', '&', '%':
		return true
	}
	switch {
	case ch >= 'a' && ch <= 'z':
		return true
	case ch >= 'A' && ch <= 'Z':
		return true
	case ch >= '0' && ch <= '9':
		return true
	case ch == '0' && ch <= '9':
		return true
	}
	return false
}
