/*
Copyright 2014 Patrick Borgeest

This file is part of HPM.

HPM is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

HPM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with HPM.  If not, see <http://www.gnu.org/licenses/>.
*/

package generator

import (
	"bytes"
	"fmt"

	"bitbucket.org/pborgeest/hpm/parser"
)

func Generate(root parser.AST) string {
	var buffer bytes.Buffer

	if root.IsNil() {
		switch root.GetInternal() {
		case "head":
			//buffer.WriteString(getTag("head", root))
			buffer.WriteString(getHead(root))
		case "body":
			buffer.WriteString(getTag("body", root))
		case "links":
			buffer.WriteString(getLinks(root))
		default:
			buffer.WriteString(getChildren(root))
		}
	} else {
		switch root.GetNodeType() {
		case parser.PAGE:
			buffer.WriteString(getPage(root))
		case parser.TITLE:
			buffer.WriteString(getTag("title", root))
		case parser.STYLE:
			buffer.WriteString(getStyle(root))
		case parser.LEFTBLOCK:
			buffer.WriteString(getLeftblock(root))
		case parser.RIGHTBLOCK:
			buffer.WriteString(getRightblock(root))
		case parser.CONTENT:
			buffer.WriteString(getContent(root))
		case parser.BLOCK:
			buffer.WriteString(getBlock(root))
		case parser.LINK:
			buffer.WriteString(getLink(root))
		case parser.SECTION:
			buffer.WriteString(getSection(root))
		default:
			buffer.WriteString(root.Token().Text())
		}
	}

	return buffer.String()
}

func getTag(tagname string, root parser.AST) string {
	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("<%s>", tagname))
	buffer.WriteString(getChildren(root))
	buffer.WriteString(fmt.Sprintf("</%s>", tagname))
	return buffer.String()
}

func getChildren(root parser.AST) string {
	var buffer bytes.Buffer
	for _, child := range root.Children() {
		buffer.WriteString(Generate(child))
	}
	return buffer.String()
}

func getPage(root parser.AST) string {
	var buffer bytes.Buffer
	buffer.WriteString(doctype())
	buffer.WriteString(getTag("html", root))
	return buffer.String()
}

func getHead(root parser.AST) string {
	var buffer bytes.Buffer
	buffer.WriteString("<head>")
	buffer.WriteString(viewportmeta())
	buffer.WriteString(getChildren(root))
	buffer.WriteString("</head>")
	return buffer.String()
}

func viewportmeta() string {
	return "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>"
}

func doctype() string {
	return "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">\n"
}

func getStyle(root parser.AST) string {
	var buffer bytes.Buffer
	buffer.WriteString("<style type=\"text/css\" media=\"screen\">@import \"")
	for _, child := range root.Children() {
		buffer.WriteString(Generate(child))
	}
	buffer.WriteString("\";</style>")
	return buffer.String()
}

func getDiv(attr string, root parser.AST) string {
	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("<div %s>", attr))
	buffer.WriteString(getChildren(root))
	buffer.WriteString("</div>")
	return buffer.String()
}

func getLeftblock(root parser.AST) string {
	return getDiv("id=\"navAlpha\"", root)
}

func getRightblock(root parser.AST) string {
	return getDiv("id=\"navBeta\"", root)
}

func getContent(root parser.AST) string {
	var buffer bytes.Buffer
	for _, child := range root.Children() {
		buffer.WriteString(fmt.Sprintf(
			"<div class=\"content\">%s</div>",
			Generate(child),
		))
	}
	return buffer.String()
}

func getLinks(root parser.AST) string {
	var buffer bytes.Buffer
	buffer.WriteString("<p>")
	children := root.Children()
	if len(children) > 0 {
		buffer.WriteString(Generate(children[0]))
		for i := 1; i < len(children); i++ {
			buffer.WriteString(separator())
			buffer.WriteString(Generate(children[i]))
		}
	}
	buffer.WriteString("</p>")
	return buffer.String()
}

func separator() string {
	nonbreakingspace := "&#160;"
	endash := "&#8211;"
	breakingspace := " "
	return fmt.Sprintf("%s%s%s", nonbreakingspace, endash, breakingspace)
}

func getLink(root parser.AST) string {
	var buffer bytes.Buffer
	children := root.Children()
	if len(children) == 2 {
		url := Generate(children[1])
		linktext := Generate(children[0])
		buffer.WriteString(fmt.Sprintf("<a href=\"%s\">%s</a>", url, linktext))
	}
	return buffer.String()
}

func getBlock(root parser.AST) string {
	var buffer bytes.Buffer
	buffer.WriteString("<p>")
	children := root.Children()
	if len(children) > 0 {
		buffer.WriteString("<h2>")
		buffer.WriteString(Generate(children[0]))
		buffer.WriteString("</h2>")
		for i := 1; i < len(children); i++ {
			buffer.WriteString(Generate(children[i]))
		}
	}
	return buffer.String()
}

func getSection(root parser.AST) string {
	var buffer bytes.Buffer
	children := root.Children()
	if len(children) == 2 {
		sectionname := Generate(children[0])
		link := Generate(children[1])
		buffer.WriteString(fmt.Sprintf(
			"<dl><dt><h3>%s</h3></dt><dd>%s</dd></dl>",
			sectionname,
			link,
		))
	}
	return buffer.String()
}
